<?php

namespace App\Form;

use App\Entity\Personne;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;

class PersonneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                'attr'=> [
                    'class' => 'form-control'
                ],
                'label' => "Votre nom",
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Saisir votre Prénom."
                    ])
                ],
            ]
        )
            ->add('prenom', TextType::class, [
                'attr'=> [
                    'class' => 'form-control'
                ],
                'label' => "Votre prénom",
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Saisir votre Prénom."
                    ])
                ],
            ])
            ->add('datedenaissance', BirthdayType::class, [
                'attr'=> [
                    'class' => 'form-control',
                ],
                'widget' => 'single_text',
                'label' => "Votre date de naisssance",
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Saisir votre date de naissance."
                    ])
                ],
            ])
            ->add('telephone', TelType::class, [
                'attr'=> [
                    'class' => 'form-control'
                ],
                'label' => "Votre numéro de téléphone",
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Saisir votre numéro de téléphone."
                    ])
                ],
            ])
            ->add('email', EmailType::class, [
                'attr'=> [
                    'class' => 'form-control'
                ],
                'label' => "Votre Email",
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => "Saisir votre email"
                    ])
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Personne::class,
        ]);
    }
}
